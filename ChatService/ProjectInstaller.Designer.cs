﻿namespace ChatService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChatServerProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ChatServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            this.ChatServiceInstaller.Description = "A chat service that uses message queues to communicate with chat clients";
            // 
            // ChatServerProcessInstaller
            // 
            this.ChatServerProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ChatServerProcessInstaller.Password = null;
            this.ChatServerProcessInstaller.Username = null;
            // 
            // ChatServiceInstaller
            // 
            this.ChatServiceInstaller.ServiceName = "ChatServer";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ChatServerProcessInstaller,
            this.ChatServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ChatServerProcessInstaller;
        private System.ServiceProcess.ServiceInstaller ChatServiceInstaller;
    }
}