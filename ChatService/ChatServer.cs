﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace ChatService
{
    public partial class ChatServer : ServiceBase
    {
        Thread threadServer = null;
        Server instanceServer = null;

        /// <summary>
        /// Constructor for ChatServer service
        /// </summary>
        public ChatServer()
        {
            InitializeComponent();
            instanceServer = new Server();
            ThreadStart threadStart = new ThreadStart(instanceServer.GetMessages);//start thread for reveiving messages
            threadServer = new Thread(threadStart);
            CanPauseAndContinue = true;
        }

        /// <summary>
        /// This function starts the ChatServer service including a log message
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            Logger.Log("Chat Server Started");
            threadServer.Start();         
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        protected override void OnPause()
        {
            Logger.Log("Chat Server Paused");
            threadServer.Suspend(); //pause the thread
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnContinue()
        {
            Logger.Log("Chat Server Paused");
            threadServer.Resume();//resume the thread
        }

        /// <summary>
        /// This function stops the ChatServer service including a log message
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStop()
        {
            Logger.Log("Chat Server Stopped");
            instanceServer.finished = true; //stop the message received loop

            // Wait for the server thread to finish
            threadServer.Join();           
        }
    }
}
