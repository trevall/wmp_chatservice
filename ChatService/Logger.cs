﻿//
// FILE : Logger.cs
// PROJECT : PROG2120 - Assignment #5
// PROGRAMMER : Attila Katona & Trevor Allain
// FIRST VERSION : 2018-11-21
// DESCRIPTION : The source code for the logging class. This class simply creates a log
//               that is viewable in Event Viewer.
//


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


namespace ChatService
{
    /// <summary>
    /// This class contains all of the logic necessary to write logs into the 
    /// windows event viewer
    /// </summary>
    public static class Logger
    {
        public static void Log(string logMessage)
        {
            EventLog serviceEventLog = new EventLog();
            if (!EventLog.SourceExists("Chat Server"))
            {
                EventLog.CreateEventSource("Chat Server", "Server Log");
            }
            serviceEventLog.Source = "Chat Server";
            serviceEventLog.Log = "Server Log";
            serviceEventLog.WriteEntry(logMessage);
        }
    }
}
